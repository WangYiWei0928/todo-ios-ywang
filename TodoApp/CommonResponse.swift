//
//  CommonResponse.swift
//  TodoApp
//
//  Created by ywang on 2020/12/16.
//

import Foundation

struct CommonResponse: Codable {
    var errorCode: Int
    var errorMessage: String
}
