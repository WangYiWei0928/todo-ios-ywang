//
//  UIViewController+.swift
//  TodoApp
//
//  Created by ywang on 2020/12/18.
//

import UIKit

extension UIViewController {
    func showErrorAlertDialog(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "閉じる", style: .default))
        present(alert, animated: true)
    }
}
