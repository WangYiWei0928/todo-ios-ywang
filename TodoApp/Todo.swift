//
//  Todo.swift
//  TodoApp
//
//  Created by ywang on 2020/12/16.
//

import Foundation

struct Todo: Codable {
    let id: Int
    var title: String
    var detail: String?
    var date: Date?
}
