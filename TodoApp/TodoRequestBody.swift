//
//  TodoRequestBody.swift
//  TodoApp
//
//  Created by ywang on 2020/12/21.
//

import Foundation

struct TodoRequestBody {
    let title: String
    let detail: String?
    let date: Date?
}
