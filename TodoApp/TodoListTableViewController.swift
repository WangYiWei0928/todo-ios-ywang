//
//  TodoListTableViewController.swift
//  TodoApp
//
//  Created by ywang on 2020/12/03.
//

import UIKit
import MaterialComponents.MaterialSnackbar

final class TodoListTableViewController: UIViewController {

    private var todos: [Todo] = []
    private var isDeleteMode = false

    @IBOutlet private var todoTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadBarButtonItem()
        fetchTodos()
    }

    private func goToEditView(todo: Todo? = nil) {
        // EditViewに画面遷移の設定
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateInitialViewController() as? TodoEditViewController else {
            return
        }
        todoEditViewController.todo = todo
        todoEditViewController.delegate = self
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    private func reloadBarButtonItem() {
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .stop : .trash
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAddButton))
        let trashButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(didTapTrashButton))
        navigationItem.rightBarButtonItems = [trashButton, addButton]
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    @objc private func didTapAddButton() {
        isDeleteMode = false
        goToEditView()
    }

    @objc private func didTapTrashButton() {
        isDeleteMode.toggle()
        reloadBarButtonItem()
    }

    private func setUpTableView() {
        // 空のセルの区切り線を消す
        todoTableView.tableFooterView = UIView()
        // 左端の余白を削除
        todoTableView.separatorInset.left = 0
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.default.show(message)
    }

    private func fetchTodos() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] error in
                self?.showErrorAlertDialog(message: error)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodos()
            },
            failure: { [weak self] error in
                self?.showErrorAlertDialog(message: error)
            }
        )
    }

    private func showDeleteAlert(todo: Todo) {
        let alert = UIAlertController(title: "「\(todo.title)」を削除します。", message: "よろしいですか", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        alert.addAction(UIAlertAction(title: "OK", style: .destructive) { _ in
            self.deleteTodo(id: todo.id)
        })
        present(alert, animated: true)
    }
}

extension TodoListTableViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, text: String) {
        showSnackbar(text: text)
    }
}

extension TodoListTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoListItem", for: indexPath)
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let todo = todos[indexPath.row]
        isDeleteMode ? showDeleteAlert(todo: todo) : goToEditView(todo: todo)
    }
}
