//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by ywang on 2020/12/16.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse

    var path: String {
        return "todos"
    }

    var parameters: Parameters? {
        return nil
    }

    var method: HTTPMethod {
        return .get
    }
}
