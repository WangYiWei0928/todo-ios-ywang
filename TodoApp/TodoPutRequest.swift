//
//  TodoPutRequest.swift
//  TodoApp
//
//  Created by ywang on 2020/12/23.
//

import Alamofire

struct TodoPutRequest: RequestProtocol {
    typealias Response = CommonResponse

    let id: Int
    let body: TodoRequestBody

    var path: String {
        return "todos/\(id)"
    }

    var parameters: Parameters? {
        var parameters = ["title": body.title, "detail": body.detail, "date": nil]
        if let date = body.date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(identifier: "UTC")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters as Parameters
    }

    var method: HTTPMethod {
        return .put
    }
}
