//
//  APIClient.swift
//  TodoApp
//
//  Created by ywang on 2020/12/16.
//

import Alamofire

final class APIClient {
    // エラーメッセージを定数化
    private let unforeseenErrorMessage = "予期せぬエラーが発生しました"
    // JSONデータのデコードと日付のフォーマット
    private var decoder: JSONDecoder {
        let formatter = DateFormatter()
        let decoder = JSONDecoder()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(identifier: "UTC")
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(formatter)
        return decoder
    }

    func call<T: RequestProtocol>(request: T, success: @escaping (T.Response) -> Void, failure: @escaping (String) -> Void) {
        let requestUrl = request.baseUrl + request.path

        AF.request(requestUrl,
                   method: request.method,
                   parameters: request.parameters,
                   encoding: request.encoding,
                   headers: request.headers
        )
        // 200番台のみ正常系として扱う
        .validate(statusCode: 200..<300)
        .responseData { response in
            guard let data = response.data else {
                failure(self.unforeseenErrorMessage)
                return
            }
            switch response.result {
            case .success:
                do {
                    let result = try self.decoder.decode(T.Response.self, from: data)
                    success(result)
                } catch {
                    failure(self.unforeseenErrorMessage)
                }
            case .failure:
                let result = try? self.decoder.decode(CommonResponse.self, from: data)
                let message = result?.errorMessage ?? self.unforeseenErrorMessage
                failure(message)
            }
        }
    }
}
