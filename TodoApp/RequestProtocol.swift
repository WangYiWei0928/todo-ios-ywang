//
//  RequestProtocol.swift
//  TodoApp
//
//  Created by ywang on 2020/12/16.
//

import Alamofire

protocol RequestProtocol {
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension RequestProtocol {
    var baseUrl: String {
        return "https://sonix-todo-api-wangyiwei.herokuapp.com/"
    }

    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }

    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}
