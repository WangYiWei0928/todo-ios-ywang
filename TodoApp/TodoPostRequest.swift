//
//  TodoPostRequest.swift
//  TodoApp
//
//  Created by ywang on 2020/12/21.
//

import Alamofire

struct TodoPostRequest: RequestProtocol {
    typealias Response = CommonResponse

    let body: TodoRequestBody

    var path: String {
        return "todos"
    }

    var parameters: Parameters? {
        var parameters = ["title": body.title]
        if let detail = body.detail {
            parameters["detail"] = detail
        }

        if let date = body.date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(identifier: "UTC")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }

    var method: HTTPMethod {
        return .post
    }
}
