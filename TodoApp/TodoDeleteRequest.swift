//
//  TodoDeleteRequest.swift
//  TodoApp
//
//  Created by ywang on 2021/01/05.
//

import Alamofire

struct TodoDeleteRequest: RequestProtocol {
    typealias Response = CommonResponse

    let id: Int

    var path: String {
        return "/todos/\(id)"
    }

    var parameters: Parameters? {
        return nil
    }

    var method: HTTPMethod {
        return .delete
    }
}
