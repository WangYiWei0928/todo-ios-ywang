//
//  TodosGetResponse.swift
//  TodoApp
//
//  Created by ywang on 2020/12/16.
//

import Foundation

struct TodosGetResponse: Codable {
    let todos: [Todo]
    let errorCode: Int
    let errorMessage: String
}
