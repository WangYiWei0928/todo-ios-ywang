//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by ywang on 2020/12/07.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, text: String)
}

final class TodoEditViewController: UIViewController {

    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var dateTextField: UITextField!
    @IBOutlet private weak var titleCountLabel: UILabel!
    @IBOutlet private weak var detailCountLabel: UILabel!

    var todo: Todo?
    weak var delegate: TodoEditViewControllerDelegate?

    private let datePicker = UIDatePicker()
    private let dateFormatter = DateFormatter()
    private let titleMaxLength = 100
    private let detailMaxLength = 1000
    private var todoTitle: String {
        titleTextField.text ?? ""
    }
    private var todoDetail: String {
        detailTextView.text
    }
    private var isTodoTitleLimitOver: Bool {
        todoTitle.count > titleMaxLength
    }
    private var isTodoDetailLimitOver: Bool {
        todoDetail.count > detailMaxLength
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDateFormatter()
        setUpRegistrationButton()
        setUpBorders()
        setUpDatePicker()
        if let todo = todo {
            setTexts(todo: todo)
        }
        changeRegistrationButtonState()
        setUpCountLabel()
        NotificationCenter.default.addObserver(self, selector: #selector(willShowKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        navigationItem.title = "TODO APP"
        titleTextField.placeholder = "タイトルを入力してください"
        detailTextView.delegate = self
        dateTextField.delegate = self
    }

    private func setUpDateFormatter() {
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
    }

    // キーボードが表示時に画面をずらす
    @objc private func willShowKeyboard(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let suggestionHeight = view.frame.origin.y + keyboardSize.height
            view.frame.origin.y -= view.frame.origin.y.isZero ? keyboardSize.height : suggestionHeight
        }
    }

    // キーボードが隠れた際にViewの位置も戻す
    @objc private func willHideKeyboard() {
        if view.frame.origin.y.isZero {
            return
        }
        view.frame.origin.y = 0
    }

    private func setUpRegistrationButton() {
        if todo != nil {
            registrationButton.setTitle("更新", for: .normal)
        }
        registrationButton.setTitleColor(.white, for: .normal)
        registrationButton.setTitleColor(.black, for: .disabled)
        registrationButton.backgroundColor = .systemBlue
        registrationButton.layer.cornerRadius = 20
    }

    private func setUpBorders() {
        // タイトルの枠線設定
        titleTextField.layer.cornerRadius = 5
        titleTextField.layer.borderWidth = 1
        titleTextField.layer.borderColor = UIColor.black.cgColor
        // 日づけの枠線設定
        dateTextField.layer.cornerRadius = 5
        dateTextField.layer.borderWidth = 1
        dateTextField.layer.borderColor = UIColor.black.cgColor
        // 詳細の枠線設定
        detailTextView.layer.cornerRadius = 5
        detailTextView.layer.borderWidth = 1
        detailTextView.layer.borderColor = UIColor.black.cgColor
    }

    private func disableRegistrationButton() {
        registrationButton.isEnabled = false
        registrationButton.backgroundColor = .systemGray
    }

    private func enableRegistrationButton() {
        registrationButton.isEnabled = true
        registrationButton.backgroundColor = .systemBlue
    }

    private func changeRegistrationButtonState() {
        // タイトルが空また上限を超え、或は詳細が上限を超える時にボタンを非活性状態にする
        if isTodoTitleLimitOver || isTodoDetailLimitOver || todoTitle.isEmpty {
            disableRegistrationButton()
        } else {
            enableRegistrationButton()
        }
    }

    @IBAction private func didClickRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        let todoRequestBody = TodoRequestBody(title: title, detail: detail, date: date)
        sendTodo(body: todoRequestBody)
    }

    private func sendTodo(body: TodoRequestBody) {
        todo?.id == nil ? addTodo(body: body) : updateTodo(body: body)
    }

    private func addTodo(body: TodoRequestBody) {
        APIClient().call(
            request: TodoPostRequest(body: body),
            success: { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.delegate?.todoEditViewControllerDidSendTodo(self, text: "登録しました")
                self.navigationController?.popViewController(animated: true)
            },
            failure: { [weak self] error in
                self?.showErrorAlertDialog(message: error)
            }
        )
    }

    private func updateTodo(body: TodoRequestBody) {
        guard let id = todo?.id else { return }
        APIClient().call(
            request: TodoPutRequest(id: id, body: body),
            success: { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.delegate?.todoEditViewControllerDidSendTodo(self, text: "更新しました")
                self.navigationController?.popViewController(animated: true)
            } ,
            failure: { [weak self] error in
                self?.showErrorAlertDialog(message: error)
            }
        )
    }

    @IBAction private func setUpTitleTextField(_ sender: Any) {
        changeRegistrationButtonState()
        // タイトルの文字数が上限を超える時に、タイトルの文字数カウントの左側の数字を赤くする
        titleCountLabel.textColor = isTodoTitleLimitOver ? .red : .black
        // タイトルの文字カウントを表示に反映させる
        titleCountLabel.text = String(todoTitle.count)
    }

    private func setUpCountLabel() {
        titleCountLabel.text = String(todoTitle.count)
        detailCountLabel.text = String(todoDetail.count)
    }

    private func setUpDatePicker() {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = Locale.current
        dateTextField.inputView = datePicker

        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        // ツールバーとボタンの配置
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(didTapDoneButton))
        let deleteItem = UIBarButtonItem(title: "削除", style: .done, target: self, action: #selector(didTapDeleteButton))
        let closeItem = UIBarButtonItem(title: "閉じる", style: .done, target: self, action: #selector(didTapCloseButton))

        toolbar.setItems([deleteItem, spaceItem, closeItem, doneItem], animated: true)
        dateTextField.inputAccessoryView = toolbar
    }

    @objc private func didTapDoneButton() {
        dateTextField.endEditing(true)
        // 「決定」を押すとdateTextFieldに日付を入れる
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func didTapDeleteButton() {
        dateTextField.endEditing(true)
        // 「削除」を押すと内容を空にする
        dateTextField.text = ""
    }

    // 「閉じる」を押すとキーボードを消す
    @objc private func didTapCloseButton() {
        dateTextField.endEditing(true)
    }

    // キーボード以外の場所をタップした時にキーボードを消す
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    private func setTexts(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        guard let date = todo.date else { return }
        dateTextField.text = dateFormatter.string(from: date)
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        changeRegistrationButtonState()
        // 詳細の文字カウント数を表示に反映させる
        detailCountLabel.text = String(todoDetail.count)
        // 詳細の文字数が上限を超える時に、詳細の文字数カウントの左側の数字を赤くする
        detailCountLabel.textColor = isTodoDetailLimitOver ? .red : .black
    }
}

extension TodoEditViewController: UITextFieldDelegate {
    // doneボタンを押すとキーボードを閉じる
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        titleTextField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // 日づけのキーボード入力や、カット/ペーストによる変更を防ぐ
        return textField != dateTextField
    }
}
