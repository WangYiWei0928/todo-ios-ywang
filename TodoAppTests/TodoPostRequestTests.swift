//
//  TodoPostRequestTests.swift
//  TodoAppTests
//
//  Created by ywang on 2021/01/08.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPostRequestTests: XCTestCase {
    private let postRequest = TodoPostRequest(body: .init(title: "a", detail: nil, date: nil))
    
    override class func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = postRequest
        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "todos")
    }
    
    func testResponse() {
        var todosPostResponse: CommonResponse?
        stub(condition: isHost("sonix-todo-api-wangyiwei.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Post Todo Request")
        APIClient().call(
            request: postRequest,
            success: { response in
                todosPostResponse = response
                expectation.fulfill()
            },
            failure: { _ in
                return
            })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosPostResponse)
            XCTAssertEqual(todosPostResponse?.errorCode, 0)
            XCTAssertEqual(todosPostResponse?.errorMessage, "")
        }
    }
}
