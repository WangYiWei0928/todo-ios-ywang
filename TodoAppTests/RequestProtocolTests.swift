//
//  RequestProtocolTests.swift
//  TodoAppTests
//
//  Created by ywang on 2021/01/12.
//

import XCTest
@testable import TodoApp

final class RequestProtocolTests: XCTestCase {
    func testInit() {
        let request = TestRequestProtocol()
        
        XCTAssertEqual(request.baseUrl, "https://sonix-todo-api-wangyiwei.herokuapp.com/")
        XCTAssertEqual(request.encoding.toJSONEncoding(), .default)
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertEqual(request.path, "todos")
        XCTAssertEqual(request.method, .get)
        XCTAssertNil(request.parameters)
    }
}
