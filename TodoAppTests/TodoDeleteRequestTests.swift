//
//  TodoDeleteRequestTests.swift
//  TodoAppTests
//
//  Created by ywang on 2021/01/08.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoDeleteRequestTests: XCTestCase {
    override class func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoDeleteRequest(id: 1)
        XCTAssertEqual(request.method, .delete)
        XCTAssertEqual(request.path, "/todos/1")
    }
    
    func testResponse() {
        var todosDeleteResponse: CommonResponse?

        stub(condition: isHost("sonix-todo-api-wangyiwei.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Delete Todo Request")

        APIClient().call(
            request: TodoDeleteRequest(id: 1),
            success: { response in
                todosDeleteResponse = response
                expectation.fulfill()
            },
            failure: { _ in
                return
            })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosDeleteResponse)
            XCTAssertEqual(todosDeleteResponse?.errorCode, 0)
            XCTAssertEqual(todosDeleteResponse?.errorMessage, "")
        }
    }
}
