//
//  TodoPutRequestTests.swift
//  TodoAppTests
//
//  Created by ywang on 2021/01/08.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPutRequestTests: XCTestCase {
    private let putRequest = TodoPutRequest(id: 1, body: .init(title: "a", detail: nil, date: nil))

    override class func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = putRequest
        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "todos/1")
    }

    func testResponse() {
        var todoPutResponse: CommonResponse?

        stub(condition: isHost("sonix-todo-api-wangyiwei.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Put Todo Request")
        APIClient().call(
            request: putRequest,
            success: { response in
                todoPutResponse = response
                expectation.fulfill()
            },
            failure: { _ in
                return
            })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoPutResponse)
            XCTAssertEqual(todoPutResponse?.errorCode, 0)
            XCTAssertEqual(todoPutResponse?.errorMessage, "")
        }
    }
}
