//
//  TestRequestProtocol.swift
//  TodoAppTests
//
//  Created by ywang on 2021/01/12.
//

import XCTest
import Alamofire
@testable import TodoApp

final class TestRequestProtocol: RequestProtocol {
    typealias Response = TodosGetResponse
    
    var baseUrl: String {
        return "https://sonix-todo-api-wangyiwei.herokuapp.com/"
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
    
    var path: String {
        return "todos"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var parameters: Parameters? {
        return nil
    }
}

extension ParameterEncoding {
    func toJSONEncoding() -> JSONEncoding? {
        self as? JSONEncoding
    }
}

extension JSONEncoding: Equatable {
    static public func ==(lhs: JSONEncoding, rhs: JSONEncoding) -> Bool {
        return lhs.options == rhs.options
    }
}
