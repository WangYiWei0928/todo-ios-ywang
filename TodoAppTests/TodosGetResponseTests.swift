//
//  TodosGetResponseTests.swift
//  TodoAppTests
//
//  Created by ywang on 2021/01/08.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodosGetRequestTests: XCTestCase {
    override class func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodosGetRequest()
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "todos")
    }

    func testResponse() {
        var todosGetResponse: TodosGetResponse?

        stub(condition: isHost("sonix-todo-api-wangyiwei.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("TodosGetResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Get Todo Request")
        APIClient().call(
            request: TodosGetRequest(),
            success: { response in
                todosGetResponse = response
                expectation.fulfill()
            },
            failure: { _ in
                return
            })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosGetResponse)
            XCTAssertEqual(todosGetResponse?.errorCode, 0)
            XCTAssertEqual(todosGetResponse?.errorMessage, "")
            XCTAssertEqual(todosGetResponse?.todos[0].title, "test1")
            XCTAssertEqual(todosGetResponse?.todos[0].detail, "test1")
            XCTAssertEqual(todosGetResponse?.todos[0].date?.toString(), "2020-12-24T00:00:00.000Z")
            XCTAssertEqual(todosGetResponse?.todos[1].title, "test2")
            XCTAssertEqual(todosGetResponse?.todos[1].detail, "test2")
            XCTAssertNil(todosGetResponse?.todos[1].date)
            XCTAssertEqual(todosGetResponse?.todos[2].title, "test3")
            XCTAssertNil(todosGetResponse?.todos[2].detail)
            XCTAssertNil(todosGetResponse?.todos[2].date)
        }
    }
}

extension Date {
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter.string(from: self)
    }
}
